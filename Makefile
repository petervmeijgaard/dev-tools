# ===========================
# Default: help section
# ===========================

info: intro do-show-commands
intro:
	@echo ""
	@echo " ██████╗ ███████╗██╗   ██╗   ████████╗ ██████╗  ██████╗ ██╗     ███████╗ "
	@echo " ██╔══██╗██╔════╝██║   ██║   ╚══██╔══╝██╔═══██╗██╔═══██╗██║     ██╔════╝ "
	@echo " ██║  ██║█████╗  ██║   ██║█████╗██║   ██║   ██║██║   ██║██║     ███████╗ "
	@echo " ██║  ██║██╔══╝  ╚██╗ ██╔╝╚════╝██║   ██║   ██║██║   ██║██║     ╚════██║ "
	@echo " ██████╔╝███████╗ ╚████╔╝       ██║   ╚██████╔╝╚██████╔╝███████╗███████║ "
	@echo " ╚═════╝ ╚══════╝  ╚═══╝        ╚═╝    ╚═════╝  ╚═════╝ ╚══════╝╚══════╝ "

# ===========================
# Main commands
# ===========================

init: \
	intro \
	do-destroy-project \
	do-build-containers \
	do-run-project \
	do-setup-hosts-file

# Docker containers
build: intro do-build-containers do-run-project
start: intro do-run-project
stop: intro do-stop-containers
down: intro do-destroy-project
restart: intro do-stop-containers do-run-project

# ===========================
# Recipes
# ===========================

do-show-commands:
	@echo "\n=== Make commands ===\n"
	@echo "Docker containers:"
	@echo "    make build    Build and start the project containers."
	@echo "    make start    Start the project containers."
	@echo "    make stop     Stop the project containers."
	@echo "    make down     Stop the project containers and clean the docker compose environment."
	@echo "    make restart  Restart the project containers."

do-show-container-info:
	@echo "\n=== Local dev environment is running ===\n"
	@echo "http://logs.dev-tools.local       Docker logs viewer"
	@echo "http://proxy.dev-tools.local      Local development proxy"

# Docker container recipes
do-run-project: do-start-containers
do-destroy-project: do-bring-containers-down

do-start-containers:
	@echo "\n=== Starting project containers ===\n"
	docker compose up -d

do-stop-containers:
	@echo "\n=== Stopping project containers ===\n"
	docker compose stop

do-bring-containers-down:
	@echo "\n=== Stopping project containers and cleaning docker compose environment ===\n"
	docker compose down -v --remove-orphans

do-build-containers:
	@echo "\n=== Building project containers ===\n"
	USERID=$$(id -u) GROUPID=$$(id -g) docker compose build

do-setup-hosts-file:
	@echo "\n=== Adding local dev-tools hosts ===\n"
	@(cat /etc/hosts | grep -q dev-tools.local \
		&& echo 'No changes: dev-tools.local already available in the hosts file.') \
	|| (docker run --rm \
		-v /etc/hosts:/etc/hosts \
		-v $$(pwd)/dev/hostnames.txt:/dev/hostnames.txt \
		alpine:latest \
		sh -c 'cat /dev/hostnames.txt >> /etc/hosts' \
		echo "Hosts successfully added")
